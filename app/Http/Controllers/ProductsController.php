<?php

namespace App\Http\Controllers;
use App\Models\Products;
use Illuminate\Support\Facades\Redis;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        
        return response()->json(Products::all());
    }

    public function show($product){
        $cachedProduct = Redis::get('product_' . $product);
        if(isset($cachedProduct)) {
            $data = json_decode($cachedProduct, FALSE);
      
            return response()->json([
                'status_code' => 201,
                'message' => 'Fetched from redis',
                'data' => $data,
            ]);
        }else {
            $data = Products::find($product);
            Redis::set('product_' . $product, $data);
      
            return response()->json([
                'status_code' => 201,
                'message' => 'Fetched from database',
                'data' => $data,
            ]);
        }
        //return response()->json(Products::find($id));
    }

    public function store(Request $request){
        $products = Products::create($request->all());

        return response()->json($products, 201);
    }

    public function update(Request $request, $product){
        $update = Products::findOrFail($product);
        $update->update($request->all());

        if($update) {

            // Delete blog_$id from Redis
            Redis::del('product_' . $product);
      
            $data = Products::find($product);
            // Set a new key with the blog id
            Redis::set('product_' . $product, $data);
      
            return response()->json([
                'status_code' => 201,
                'message' => 'Product updated',
                'data' => $data,
            ]);
        }
        //return response()->json($products, 200);
    }

    public function destroy($product){
        Products::findOrFail($product)->delete();
        Redis::del('product_' . $product);
        
        return response('Deleted Successfully', 200);
    }
}
