<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        //factory(\App\Products::class, 50)->create();
        //Products::factory()->times(50)->create();
        //factory(Products::class, 50)->create();
        //\App\Models\Products::factory(10)->create();
        factory(\App\Models\Products::class, 3)->create();
    }
}
